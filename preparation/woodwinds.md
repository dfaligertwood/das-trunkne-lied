# Woodwinds

In general "à 2" is not consistently handled. Sometimes it's with the label
and only one voice written, sometimes it's written as two unisono voices
(with stems up and down).  
I suggest to make that consistent and use the "à 2" approach consistently
because that will use less space and ink in the score.

## Flutes

Two flutes on one staff. Default `\partcombine` job.  
Third flute on separate staff. All in a `PianoStaff`

## Oboes

Two oboes on one staff. Default `\partcombine` job.  
English horn on separate staff. All in a `PianoStaff`

## Bassoons

I admit I have only looked at the first 30 pages, but I'm sure it's the same
as with flutes and oboes (2 bassoons (on one system) plus contra bassoon.

## Clarinets

- two clarinets on one staff, somtimes written as voices, sometimes as chords
- bass clarinet on second staff
- everything in a PianoStaff
- "à 2" inconsistently used. p.24 starts with "à 2", p.25 continues with two
  written voices.
- p. 28 the clarinets on two staves in a PianoStaff, the bass clarinet extra  
  This is inconsistent, and I'm not sure how to deal with it.
  Probably we should use a PianoStaff for the two clarinets and simply write
  the bass clarinet on its own staff. Or rather use a PianoStaff for all three.
  Or (my favorite) don't follow this two staff splitting as it isn't clear why
  *these* systems should be split while others are not. 

Summary: looks like (mostly) a default partcombine job. I don't know about the
subtleties of performance indications etc. that Antonio describes in his
tutorial, but we'll have to see that.

I suggest starting with consequently combining the two clarinets on one staff.
[EDIT]: No, the split sections *have* to be on separate staves because of
heavy voice-crossing. So we have to deal with that.
