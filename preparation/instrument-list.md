# Oskar Fried: Das trunkne Lied -- Instrument List

### String instruments

- Vl.: 16 first violins
- Vl.: 16 second violins
- Br.: 12 violas
- Vc.: 12 celli
- C.-B.: 8 double basses  
  (*at least* 4 with lower C)
  
### Woodwinds

- Fl.: 3 flutes  
  (3rd also piccolo)
- Ob.: 2 Oboes
- AltOb.: Alto Oboe (English horn)
- Cl.: 2 clarinets
- Bcl.: Bass clarinet in A and B  
  (it's not clear whether this goes for the bass clarinet only)
- Fg.: 2 bassoons
- Cfg.: Contra bassoon

### Brass

- Hr.: 4 horns
- Tromp.: 4 trumpets
- Tenorpos.: 2 tenor trombones
- Basspos.: Quart bass trombone (?) "Quart Bassposaune"
- C.Basstbe.: Double bass tuba (?)

### Percussion

- Pken.: 2 pairs of timpani
- Trg.: Triangle
- Bcken.: 1 pair of cymbals
- Tamtam
- bass drum

### String instruments

- 1 harp (two players (?))

### Solo voices

- Soprano
- Alto
- Bass

### Choir

- 8 solo sopranos
- 8 solo altos
- SATB choir