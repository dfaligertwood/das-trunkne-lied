%{
  parts/doublebass/21.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 21 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XXI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*2 \origBreak | %26
  R1*6 \origBreak | %27
  \mark \default
}

\compileSegment \XXI
