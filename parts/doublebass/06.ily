%{
  parts/doublebass/06.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 06 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

VI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak | %7
  R2.*4
  \mark \default
}

\compileSegment \VI
