%{
  parts/doublebass/89.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 89 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXXXIX = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*7 \origBreak |%145
  \mark \default
}

\compileSegment \LXXXIX
