%{
  parts/doublebass/55.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 55 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LV = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*5 \origBreak |%88
  R1*6 \origBreak |%89
  \mark \default
}

\compileSegment \LV
