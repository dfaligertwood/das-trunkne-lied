%{
  parts/doublebass/70.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 70 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXX = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*4 \origBreak |%118
  R2.*4
  \mark \default
}

\compileSegment \LXX
