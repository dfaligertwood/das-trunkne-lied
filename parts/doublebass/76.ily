%{
  parts/doublebass/76.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 76 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXXVI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2.*5 \origBreak |%126
  R2.*8 \origBreak |%127
  \time 4/4 R1 \time 3/4 R2.*2
  \mark \default
}

\compileSegment \LXXVI
