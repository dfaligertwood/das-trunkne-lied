%{
  parts/doublebass/56.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 56 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LVI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*6 \origBreak |%90
  R1*7
  \mark \default
}

\compileSegment \LVI
