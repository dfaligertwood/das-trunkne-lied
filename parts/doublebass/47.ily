%{
  parts/doublebass/47.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 47 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XLVII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2. \time 4/4 R1 \origBreak |%74
  \time 3/4 R2.*8 \origBreak |%75
  R2.
  \mark \default
}

\compileSegment \XLVII
