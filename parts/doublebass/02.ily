%{
  parts/doublebass/02.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 02 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

II = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*5 \time 5/4 R4*5 \time 3/4
  \mark \default
}

\compileSegment \II
