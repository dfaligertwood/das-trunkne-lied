%{
  parts/doublebass/61.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 61 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/2
  R1.*3 \origBreak |%100
  \time 6/4 R1. \time 3/2 R1. \time 6/4 R1. \time 3/2 R1. \origBreak |%101
  \time 6/4 R1. \time 3/2 R1. \time 6/4 R1. \origBreak |%102
  \time 3/2 R1.
  \mark \default
}

\compileSegment \LXI
