%{
  parts/doublebass/11.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 11 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

XI = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*2 \origBreak | %13
  R2.*8 \origBreak | %14
  \mark \default
}

\compileSegment \XI
