%{
  parts/doublebass/04.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 04 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

IV = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak | %5
  R2.*7
  \mark \default
}

\compileSegment \IV
