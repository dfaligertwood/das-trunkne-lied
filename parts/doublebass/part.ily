%{
   parts/doublebass/part.ily

   Concatenates the segments of part:

      Contrabaesse.
      =============

  This is a generated file. Do not edit.

%}

% include default empty segments
\include "global/empty-segments.ily"

% include all found segment files

\include "01.ily"

% concatenate all segments to \part
\include "makescore/concat-segments.ily"

% assign part to specific variable
doublebass = \part
