%{
  parts/doublebass/88.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 88 from the
  doublebass (Contrabaesse.) part

%}

\include "makescore/compile-segment.ily"

LXXXVIII = \relative c {
  \set Staff.instrumentName = "Contrabaesse."
  \set Staff.shortInstrumentName = "Cb."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4 R2. \origBreak |%143
  R2.*4 \origBreak |%144
  \mark \default
}

\compileSegment \LXXXVIII
