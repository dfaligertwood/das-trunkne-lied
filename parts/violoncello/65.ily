%{
  parts/violoncello/65.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 65 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/2 R1.*2 \origBreak |%109
  R1.*3
  \mark \default
}

\compileSegment \LXV
