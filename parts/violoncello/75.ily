%{
  parts/violoncello/75.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 75 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*2 \origBreak |%125
  R2. \time 2/4 R2
  \mark \default
}

\compileSegment \LXXV
