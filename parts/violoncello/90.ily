%{
  parts/violoncello/90.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 90 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XC = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*5 \bar "|." |%146
}

\compileSegment \XC
