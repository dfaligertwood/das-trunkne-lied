%{
  parts/violoncello/36.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 36 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXXVI = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 12/8 R1.*2 \origBreak |%53
  \time 6/8 R2. \time 12/8 R1.*2 \origBreak |%54
  \mark \default
}

\compileSegment \XXXVI
