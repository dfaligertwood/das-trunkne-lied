%{
  parts/violoncello/37.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 37 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXXVII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 12/8
  R1.*2 \time 6/8 R2. \origBreak |%55
  \time 12/8 R1.*2 \time 5/8 R8*5 \origBreak |%56
  \mark \default
}

\compileSegment \XXXVII
