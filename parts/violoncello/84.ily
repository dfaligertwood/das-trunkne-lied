%{
  parts/violoncello/84.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 84 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXXIV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*6 \time 4/4 R1*2 \origBreak |%137
  \mark \default
}

\compileSegment \LXXXIV
