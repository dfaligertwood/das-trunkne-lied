%{
  parts/violoncello/04.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 04 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

IV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*3 \origBreak | %5
  R2.*7
  \mark \default
}

\compileSegment \IV
