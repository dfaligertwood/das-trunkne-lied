%{
  parts/violoncello/18.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 18 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XVIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1*3 \origBreak | %22
  R1
  \mark \default
}

\compileSegment \XVIII
