%{
  parts/violoncello/32.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 32 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXXII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*7 \origBreak | %48
  \mark \default
}

\compileSegment \XXXII
