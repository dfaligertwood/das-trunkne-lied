%{
  parts/violoncello/82.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 82 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

LXXXII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2.*2 \origBreak |%134
  R2. \time 2/4 R2 \time 3/4 R2.*5 \origBreak |%135
  \time 5/4 R4*5 \time 3/4 R2.*2
  \mark \default
}

\compileSegment \LXXXII
