%{
  parts/violoncello/34.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 34 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXXIV = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 3/4
  R2. \origBreak | %50
  R2.*6 \origBreak | %51
  R2.*4
  \mark \default
}

\compileSegment \XXXIV
