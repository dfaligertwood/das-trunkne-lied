%{
  parts/violoncello/23.ly

  This is a generated file, do not edit content
  outside the variable definition!

  This contains segment 23 from the
  violoncello (Violoncello.) part

%}

\include "makescore/compile-segment.ily"

XXIII = \relative c {
  \set Staff.instrumentName = "Violoncello"
  \set Staff.shortInstrumentName = "Vc."
  % Replace \key and \clef with actual values
  \key c \major
  \clef treble

  \time 4/4
  R1 \origBreak | %29
  R1*4 \origBreak | %30
  R1*3
  \mark \default
}

\compileSegment \XXIII
