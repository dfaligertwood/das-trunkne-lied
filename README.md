Das trunkne Lied
================

Oskar Fried: Das Trunkene Lied

---

New edition of Oskar Fried "Das Trunkne Lied",  
commissioned by: [„das junge orchester NRW“](www.djo-nrw.de)

---

For installation and other notes please refer to the [Wiki](https://bitbucket.org/beautifulscores/das-trunkne-lied/wiki/Home) pages

---

Contact:  
Urs Liska <mailto:beautifulscores@ursliska.de>