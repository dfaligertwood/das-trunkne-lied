%{
   library/ly/layout/keep-original-breaks-score.ily
   
   map \origBreak and \origLineBreak to useful functions to
   keep the original page layout of the score while entering music
   
   The manual breaks are only entered in empty-segments.ily
   because that is used in each part or score anyway.
   
%}


% Initialize first page number
\paper {
  first-page-number = 3
}

% Enter a manual line break (for each pagebreak in the model)
origBreak = \pageBreak

origLineBreak = \break
