%{
   library/ly/layout/keep-original-breaks.ily
   
   map \origBreak and \origLineBreak to useful functions to
   keep the original page layout of the score while entering music
   
   The manual breaks are only entered in empty-segments.ily
   because that is used in each part or score anyway.
   
%}


% Initialize first page number
pagenum = #3

% Enter a manual line break (for each pagebreak in the model)
% Increment the pagenum and label the instrument name with it
% (which should only affect the 'real' staff of the example score)
origBreak = #(define-music-function (parser location)
               ()
               (set! pagenum (+ 1 pagenum))
               #{ \break
                  \set Staff.shortInstrumentName = #(number->string pagenum) #}
               )

% Just a line break without pagebreak.
% Remove the shortInstrumentName of the new line as an indicator
origLineBreak = {
  \break
  \set Staff.shortInstrumentName = ""
}
