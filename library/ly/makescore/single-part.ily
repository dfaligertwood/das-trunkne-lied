%{
   library/ly/makescore/singlePart.ily
   
   This is just a stub for developing
   the real score structure
   
   This file will be a template, from which 
   a Python script will generate files for
   the individual parts
   (along with the directory fill of empty segments)

%}

% include layout options for printing parts
\include "layout/part-layout.ily"

% score block to be included and populated by the 
%% actual part file
\score {
  
  \new Staff \part
  
  \layout {}
  \midi {}
}
