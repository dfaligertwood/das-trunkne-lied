\version "2.17.3"

%{
  Markup command for local performance indications:
  - technical indications (sordini, change ...)
  - expressive indications
  usage: \markup \techind "Text" or \markup \exprind "Text"
%}
#(define-markup-command (techind layout props text) (markup?)
  "Print an technical performance indication (like sordini or instrument change."
  (interpret-markup layout props
    (markup #:italic 
            #:with-color (x11-color "blue")
            text)))

#(define-markup-command (exprind layout props text) (markup?)
  "Print an expressive performance indication."
  (interpret-markup layout props
    (markup #:italic 
            #:with-color (x11-color "brown")
            text)))
