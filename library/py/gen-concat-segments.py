#!/usr/bin/env python

# read input file marksAndBreaks.ily and process it:
# number the variable with roman numerals
# fill the start of each variable with
# - a c major key (so one sees that one has to enter the correct value)
# - the last \time command (so the variable explicitely starts with a timesig)
#   - this is omitted if the variable already starts with a timesig

from romannumerals import int2roman
import  os, sys

dest_file = []

def main():
    global source_file
    generate_segments(1, 90)
    write_output_file('../../music/global/concatSegments.ily')
    for line in dest_file:
    	 print line

def generate_segments(first = 1, last = 3999):
	dest_file.append('part = {')
	for seg in range(first, last + 1):
		dest_file.append('  \\' + int_to_roman(seg))
	dest_file.append('}')

def write_output_file(out_file):
    global dest_file
    print 'Write generated file to ' + out_file
    fout = open(out_file, 'w')
    for line in dest_file:
        fout.write(line + '\n')
    fout.close()


# ####################################
# Finally launch the program
if __name__ == "__main__":
    main()
