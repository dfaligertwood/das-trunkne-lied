#!/usr/bin/env python

# create an empty instrumental part for "Das trunkne Lied"

from __future__ import unicode_literals

import  os, sys
from util import check_path
from romannumerals import *

dest_file = []
empty_segments_file = []


def main():
    global varname, inst_name, inst_short_name
    global part_file,  part_dir, part_concat_file
    
    check_path()
    
    # read and check name arguments
    try:
        varname = sys.argv[1]
        inst_name = sys.argv[2]
        inst_short_name = sys.argv[3]
    except IndexError:
        print 'usage: gen-empty-part varename inst_name inst_short_name'
        exit(1)
    
    # basic validity check for filename
    if varname.find(' ') > 0:
        print 'Spaces in filename are not allowed. Aborting'
        exit(1)
    
    # ask user for confirmation about arguments
    print 'You asked me to set up the instrumental part'
    print '- Instrument name:', inst_name
    print '- Short name:', inst_short_name
    print '- filename (no spaces):', varname
    print ''
    chk = raw_input('Is this correct (everything but ''y'' aborts)? ')
    if not chk == 'y':
        print 'Script aborted by user'
        exit(1)
    
    # construct global file/dir name variables
    part_file = os.path.join('parts', varname + '.ly')
    part_dir = os.path.join('parts',  varname, '')
    part_concat_file = os.path.join(part_dir, 'part.ily')
    
    # Check if part subdirectory already exists.
    # TODO: Implement careful ways to update an existing
    # part subdir. For now we just abort.
    if os.path.isdir(part_dir):
        print 'dir found:', varname
        print 'Not smart enough yet to continue. Aborting'
        #exit(1)
    else:
        print 'Create dir:',  varname
        os.mkdir(part_dir)
    
    generate_part_file(part_file)
    
    generate_part_concat_file(part_concat_file)
        
    generate_part_segments(part_dir)
    
    exit(0)
    

def find_roman_variable(stringlist, start_line = 0):
    varblock = []
    for i in range(start_line, len(stringlist)):
        line = stringlist[i].lstrip()
        space = line.find(' =')
        if space >= 0:
            segname = line[:space]
            if is_roman_numeral(segname):
                segname = roman2int(segname)
                if segname < 10:
                    segname = '0' + str(segname)
                else:
                    segname = str(segname)
                for j in range(i,  len(stringlist)):
                    varblock.append(stringlist[j])
                    if varblock[-1][0] == '}':
                        return (j, segname,  varblock)
    return (0, '', [])
    
def generate_part_concat_file(filename):
    out_file = []
    filelist = os.listdir(part_dir)
    filelist.sort()
    for entry in filelist:
        name, ext = os.path.splitext(entry)
        try:
            num = int(name)
            if ext == '.ily':
                out_file.append('\\include \"' + entry + '\"\n')
        except:
            pass
    
    fout = open(filename, 'w')
    fout.write('%{\n')
    fout.write('   parts/' + varname + '/part.ily\n\n')
    fout.write('   Concatenates the segments of part:\n\n')
    fout.write('      ' + inst_name + '\n')
    fout.write('      ' + '=' * len(inst_name) + '\n\n')
    fout.write('  This is a generated file. Do not edit.\n\n')
    fout.write('%}\n\n')
    fout.write('% include default empty segments\n')
    fout.write('\include \"global/empty-segments.ily\"\n\n')
    fout.write('% include all found segment files\n\n')
    
    for line in out_file:
        fout.write(line)
    
    fout.write('\n% concatenate all segments to \part\n')
    fout.write('\\include \"makescore/concat-segments.ily\"\n\n')
    fout.write('% assign part to specific variable\n')
    fout.write(varname + ' = \\part\n')
    fout.close()

def generate_part_file(filename):
    fout = open(filename, 'w')
    fout.write('%{\n')
    fout.write('   ' + filename + '\n\n')
    fout.write('   Standalone file to compile part:\n\n')
    fout.write('      ' + inst_name + '\n')
    fout.write('      ' + '=' * len(inst_name) + '\n\n')
    fout.write('  This is a generated file. Do not edit.\n\n')
    fout.write('%}\n\n')
    fout.write('% Load project-wide libraries, settings and values\n')
    fout.write('\include \"init-edition.ily\"\n\n')
    fout.write('% Add instrument specific header entries\n')
    fout.write('\header {\n')
    fout.write('  instrument = \"' + inst_name + '\"\n')
    fout.write('}\n\n')
    fout.write('% Include layout defaults for instrumental parts\n')
    fout.write('\\include \"layout/part-layout.ily\"\n\n')
    fout.write('% Include music definition (\\part variable)\n')
    fout.write('\include \"' + varname + '/part.ily\"\n\n')
    fout.write('% Include default settings and a score block\n')
    fout.write('\include \"makescore/single-part.ily\"\n')
    fout.close()
    
def generate_part_segments(directory):
    global empty_segments_file
    
    fin = open(os.path.join('library', 'ly', 'global', 'empty-segments.ily'))
    empty_segments_file = fin.readlines()
    
    os.chdir(directory)
    
    var_tup = find_roman_variable(empty_segments_file)
    input_index = var_tup[0]
    segname = var_tup[1]
    varblock = var_tup[2]
    while input_index > 0:
        # iterate over all segments in the input file
        
        # reset variable
        existing_variable = []
        # write out one .ily file for each segment
        if os.path.exists(segname + '.ily'):
            fin = open(segname + '.ily', 'r')
            in_file = fin.readlines()
            fin.close()
            var_tup_existing = find_roman_variable(in_file)
            existing_variable = var_tup_existing[2]
        
        fout = open(segname + '.ily', 'w')
        fout.write('%{\n')
        fout.write('  parts/' + varname + '/' + segname + '.ly\n\n')
        fout.write('  This is a generated file, do not edit content\n')
        fout.write('  outside the variable definition!\n\n')
        fout.write('  This contains segment ' + segname + ' from the\n')
        fout.write('  ' + varname + ' (' + inst_name + ') part\n\n')
        fout.write('%}\n\n')
        fout.write('\\include \"makescore/compile-segment.ily\"\n\n')
        
        
        if existing_variable:
            for line in existing_variable:
                fout.write(line)
        else:
            fout.write(int2roman(int(segname)) + ' = \\relative c'' {\n')
            del varblock[0]
            fout.write('  \set Staff.instrumentName = \"' + inst_name + '\"\n')
            fout.write('  \set Staff.shortInstrumentName = \"' + inst_short_name + '\"\n')
            fout.write('  % Replace \\key and \\clef with actual values\n')
            fout.write('  \\key c \\major\n')
            fout.write('  \clef treble\n\n')
            for line in varblock:
                fout.write(line)
        
        fout.write('\n')
        fout.write('\\compileSegment \\' + int2roman(int(segname)) + '\n')
        fout.close()

        # find next segment (if any)
        var_tup = find_roman_variable(empty_segments_file, input_index)
        input_index = var_tup[0]
        segname = var_tup[1]
        varblock = var_tup[2]
        

# ####################################
# Finally launch the program
if __name__ == "__main__":
    main()
